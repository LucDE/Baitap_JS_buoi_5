// Bài 5.1
let standard_mark_input = []; //Mảng chứa điểm sàn
let standard_mark; // Điểm sàn
let array_mark_input = []; //Mảng chứa điểm thi
let sum_mark_input; // Tổng điểm thi
let area_input; // Khu vực
let object_input; // Đối tượng
let area_case; // Giá trị được chọn của khu vực
let object_case; // Giá trị được chọn của đối tượng
let area_mark; // Điểm cộng khu vực
let object_mark; // Điểm cộng đối tượng
let prioritized_mark; //Điểm ưu tiên
let final_sum_result; // Điểm tổng kết
// get the input value into an array
enter_put_value_into_array("standard_mark", standard_mark_input);
//Line code below show the definition of function
// console.log(enter_put_value_into_array.toString());
enter_put_value_into_array("mark", array_mark_input);
function result_exam() {
  var length_of_array_mark = array_mark_input.length;
  var length_of_standard_mark = standard_mark_input.length;
  if (
    length_of_array_mark == 3 &&
    length_of_standard_mark == 1 &&
    Number(array_mark_input[0]) > 0 &&
    Number(array_mark_input[1]) > 0 &&
    Number(array_mark_input[2]) > 0
  ) {
    standard_mark = Number(standard_mark_input[0]);
    console.log("Điểm chuẩn: ", standard_mark);
    sum_mark_input =
      Number(array_mark_input[0]) +
      Number(array_mark_input[1]) +
      Number(array_mark_input[2]);
    console.log("Tổng điểm thi: ", sum_mark_input);
    area_input = dom_id("area");
    object_input = dom_id("object");
    area_case = area_input.options[area_input.selectedIndex].value;
    object_case = Number(
      object_input.options[object_input.selectedIndex].value
    );
    console.log("case_khu vực: ", area_case);
    console.log("case đối tượng: ", object_case);
    switch (area_case) {
      case "X":
        area_mark = 0;
        break;
      case "A":
        area_mark = 2;
        break;
      case "B":
        area_mark = 1;
        break;
      case "C":
        area_mark = 0.5;
        break;
    }
    console.log("điểm khu vực: ", area_mark);
    switch (object_case) {
      case 0:
        object_mark = 0;
        break;
      case 1:
        object_mark = 2.5;
        break;
      case 2:
        object_mark = 1.5;
        break;
      case 3:
        object_mark = 1;
        break;
    }
    console.log("điểm đối tượng: ", object_mark);
    prioritized_mark = area_mark + object_mark;
    console.log("điểm ưu tiên: ", prioritized_mark);
    final_sum_result = sum_mark_input + prioritized_mark;
    console.log("điểm tổng kết: ", final_sum_result);
    if (
      Number(array_mark_input[0]) != 0 &&
      Number(array_mark_input[1]) != 0 &&
      Number(array_mark_input[2]) != 0
    ) {
      if (final_sum_result >= standard_mark) {
        put_text_into_id(
          "alert1",
          `Tổng điểm: ${final_sum_result}. Kết quả: Đậu!`
        );
      } else {
        put_text_into_id(
          "alert1",
          `Tổng điểm: ${final_sum_result}. Kết quả: Rớt.`
        );
      }
    } else {
      put_text_into_id(
        "alert1",
        `Tổng điểm: ${final_sum_result}. Kết quả: Rớt.`
      );
    }
    for (i = 0; i <= length_of_array_mark; i++) {
      array_mark_input.pop();
    }
    for (i = 0; i <= length_of_standard_mark; i++) {
      standard_mark_input.pop();
    }
  } else {
    put_text_into_id(
      "alert1",
      `Vui lòng nhập đúng các giá trị <br> 1 điểm chuẩn và 3 điểm thi`
    );
    for (i = 0; i <= length_of_array_mark; i++) {
      array_mark_input.pop();
    }
    for (i = 0; i <= length_of_standard_mark; i++) {
      standard_mark_input.pop();
    }
  }
}
function reset_input() {
  put_text_into_id("alert1", ``);
}
//Bài 5.2: Tính tiền điện
//Rút gọn để chỉnh sửa
var i = 500; //hệ số tiền điện/1kW
var p = 50; // hệ số mốc kW
var Z = [p, p, 2 * p, 3 * p];
let Y = [i, 1.3 * i, 1.7 * i, 2.2 * i, 2.6 * i]; //Các mốc tiền điện/1 kW
function caculate_electric_bill() {
  var x; //Số kW
  var X; //Mốc kW
  var a;
  var electric_bill = 0;
  x = dom_id_value("kW");
  if (x > 0) {
    if (x >= 7 * p) {
      a = Z.length + 1;
      X = [Z[0], Z[1], Z[2], Z[3], x - 7 * p];
    } else if (x >= 4 * p) {
      a = Z.length;
      X = [Z[0], Z[1], Z[2], x - 4 * p, 0];
    } else if (x >= 2 * p) {
      a = Z.length - 1;
      X = [Z[0], Z[1], x - 2 * p, 0, 0];
    } else if (x >= p) {
      a = Z.length - 2;
      X = [Z[0], x - p, 0, 0, 0];
    } else {
      a = Z.length - 3;
      X = [x, 0, 0, 0, 0];
    }
    for (i = 0; i < a; i++) {
      electric_bill += Y[i] * X[i];
    }
    put_text_into_id(
      "alert2",
      `Số tiền phải thanh toán là: ${electric_bill.toLocaleString()} VNĐ.`
    );
  } else {
    put_text_into_id("alert2", `Vui lòng nhập lại.`);
  }
  dom_id_value_return_0("kW");
}

// let Y = [500, 650, 850, 1100, 1300]; //Các mốc tiền điện/1 kW
// function caculate_electric_bill() {
//   var x; //Số kW
//   var X; //Mốc kW
//   var a;
//   var electric_bill = 0;
//   x = dom_id_value("kW");
//   // X = [x, x - 50, x - 100, x - 200, x - 350];
//   if (x > 0) {
//     if (x >= 350) {
//       a = 5;
//       X = [50, 50, 100, 150, x - 350];
//     } else if (x >= 200) {
//       a = 4;
//       X = [50, 50, 100, x - 200, 0];
//     } else if (x >= 100) {
//       a = 3;
//       X = [50, 50, x - 100, 0, 0];
//     } else if (x >= 50) {
//       a = 2;
//       X = [50, x - 50, 0, 0, 0];
//     } else {
//       a = 1;
//       X = [x, 0, 0, 0, 0];
//     }
//     for (i = 0; i < a; i++) {
//       electric_bill += Y[i] * X[i];
//     }
//     put_text_into_id(
//       "alert2",
//       `Số tiền phải thanh toán là: ${electric_bill.toLocaleString()} VNĐ.`
//     );
//   } else {
//     put_text_into_id("alert2", `Vui lòng nhập lại`);
//   }
//   dom_id_value_return_0("kW");
// }

// -----------------------------
// // Tạo tỉ lệ tiền điện/1kW và tỉ lệ các mốc
// //Tạo 1 mảng chứa các mốc tiền điện
// var i = 500; //hệ số tiền điện/1kW
// var p = 50; // hệ số mốc kW
// var Z = [p, p, 2 * p, 3 * p];
// let Y = [i, 1.3 * i, 1.7 * i, 2.2 * i, 2.6 * i]; //Các mốc tiền điện/1 kW
// function caculate_electric_bill() {
//   var x; //Số kW
//   var X; //Mốc kW
//   var a;
//   var electric_bill = 0;
//   x = dom_id_value("kW");
//   // X = [x, x - 50, x - 100, x - 200, x - 350];
//   if (x > 0) {
//     if (x >= 7 * p) {
//       a = 5;
//       X = [p, p, 2 * p, 3 * p, x - 7 * p];
//     } else if (x >= 4 * p) {
//       a = 4;
//       X = [p, p, 2 * p, x - 4 * p, 0];
//     } else if (x >= 2 * p) {
//       a = 3;
//       X = [p, p, x - 2 * p, 0, 0];
//     } else if (x >= p) {
//       a = 2;
//       X = [p, x - p, 0, 0, 0];
//     } else {
//       a = 1;
//       X = [x, 0, 0, 0, 0];
//     }
//     for (i = 0; i < a; i++) {
//       electric_bill += Y[i] * X[i];
//     }
//     put_text_into_id(
//       "alert2",
//       `Số tiền phải thanh toán là: ${electric_bill.toLocaleString()} VNĐ.`
//     );
//   } else {
//     put_text_into_id("alert2", `Vui lòng nhập lại.`);
//   }
//   dom_id_value_return_0("kW");
// }
